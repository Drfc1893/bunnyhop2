#include "AssetManager.h"


std::map<std::string, sf::Texture> AssetManager::textures = std::map<std::string, sf::Texture>();
std::map < std::string, sf::Font>AssetManager::fonts = std::map<std::string, sf::Font>();
std::map<std::string, sf::SoundBuffer> AssetManager::soundBuffers = std::map<std::string, sf::SoundBuffer>();

sf::Texture & AssetManager::RequestTexture(std::string fileName)
{
	// TODO: insert return statement here
	// TODO: check if the asset is already loaded 
	auto pairFound = textures.find(fileName);

	std::map<std::string, sf::Texture>::iterator;


	//TODO:DONE if it is already loaded, return the existing assets
	if (pairFound != textures.end())
	{
		return pairFound->second;
	}

	//TODO:DONE if it is not already loaded, load it and return the new assets
	else
	{
		sf::Texture& newTexture = textures[fileName];
		newTexture.loadFromFile(fileName);
		return newTexture;
	}

}

//sound code 
sf::SoundBuffer& AssetManager::RequestSoundBuffer(std::string  fileName)
{
	//check if the asset is already locked 
	auto pairFound = soundBuffers.find(fileName);

	//if it is already loaded, return the existing asset
	if (pairFound != soundBuffers.end())
	{
		return pairFound->second;
	}
	//fi it is not already loaded, load it and return the new asset 
	else
	{
		sf::SoundBuffer& newSoundBuffer = soundBuffers[fileName];
		newSoundBuffer.loadFromFile(fileName);
		return newSoundBuffer;
	}
}

//font code
sf::Font& AssetManager::RequestFont(std::string fileName)
{
	//chekc fi the asset is already loaded 
	auto pairFound = fonts.find(fileName);

	//if it is already loaded, return the existing assets 
	if (pairFound != fonts.end())
	{
		return pairFound->second;
	}
	//if it is not already loaded, load it and return the bnew asset
	else
	{
		sf::Font& newFont = fonts[fileName];
		newFont.loadFromFile(fileName);
		return newFont;
	}

}



