#pragma once
#include <SFML/Graphics.hpp>
#include "LevelScreen.h"

class Game
{

public:
	Game();

	void RunGameLoop();

	void Input();
	void Update();
	void Draw();

	//getter 
	sf::RenderWindow& GetWindow();

private:

	sf::RenderWindow window;
	sf::Clock gameClock;
	LevelScreen levelScreenInstance;
};

