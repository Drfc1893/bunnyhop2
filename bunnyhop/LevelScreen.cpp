#include "LevelScreen.h"
#include <iostream>
#include <fstream>
#include "Game.h"
#include <stdlib.h>

LevelScreen::LevelScreen(Game* newGamePointer)
	:playerInstance(newGamePointer->GetWindow().getSize())
	, gamePointer(newGamePointer)
	//,platformInstance()
	//,platformInstance2()
	, camera(newGamePointer->GetWindow().getDefaultView())
	, platforms()
	,platformGap(50)
	,platformGapIncrease(3)
	,highestPlatform(0)
	,platformBuffer(100)
{
	//todo: Calculate center of the person 
	//done 
	Platform platformInstance;
	sf::Vector2f newPosition;
	newPosition.x = newGamePointer->GetWindow().getSize().x / 2;
	newPosition.y = newGamePointer->GetWindow().getSize().y / 2;
	//TODO: Calculate position of platform to be centered
	//done
	newPosition.x -= platformInstance.getHitBox().width / 2;
	newPosition.y -= platformInstance.getHitBox().height / 2;

	//TODO: Add to the y position to lower the plaform a bit 
	const float PLATFORM_OFFSET = 250;//this can be adjusted as needed 
	newPosition.y += PLATFORM_OFFSET;

	//TODO: Set the new position of the plaform 
	//done
	platformInstance.SetPosition(newPosition);

	//TODO: Calculate position of platform to be centered
	//done
	//newPosition.x -= platformInstance2.getHitBox().width / 2;
	//newPosition.y -= platformInstance2.getHitBox().height / 2;

	//TODO: Add to the y position to lower the plaform a bit 
	const float PLATFORM2_OFFSET = -250;//this can be adjusted as needed 
	newPosition.y += PLATFORM2_OFFSET;

	//platformInstance2.SetPosition(newPosition);

	platforms.push_back(platformInstance);
	//populate the rest of the platform
	highestPlatform = newPosition.y;
	//place platforms until the hichest one is outside the camera
	float cameraTop = camera.getCenter().y - camera.getSize().x / 2.0f;
	while (highestPlatform > cameraTop - platformBuffer)
	{
		//TODO: add platform
		//done
		AddPlatform();
	}
}

void LevelScreen::Input()
{
	playerInstance.Input();
}

void LevelScreen::Update(sf::Time frameTime)
{
	playerInstance.Update(frameTime);

	for (int i = 0; i < platforms.size(); i++)
	{
		playerInstance.HandleSolidCollision(platforms[i].getHitBox());
	}

	//playerInstance.HandleSolidCollision(platformInstance.getHitBox());

	//playerInstance.HandleSolidCollision(platformInstance2.getHitBox());

	//place a new platform if needed 
	float cameraTop = camera.getCenter().y - camera.getSize().x / 2.0f;
	if (highestPlatform > cameraTop - platformBuffer)
	{
		AddPlatform();
	}

	//if bottom platform is off screen, remove it 
	if (platforms[0].getHitBox().top > camera.getCenter().y + camera.getSize().y / 2)
	{
		platforms.erase(platforms.begin());//erase the firts element of the vector 
	}
}

void LevelScreen::DrawTo(sf::RenderTarget& target)
{

	//ste up camera 
	//update camera position 
	sf::Vector2f currentViewCenter = camera.getCenter();
	float playerCenterY = playerInstance.getHitBox().top + playerInstance.getHitBox().height / 2;
	if (playerCenterY < currentViewCenter.y)
	{
		camera.setCenter(currentViewCenter.x, playerCenterY);
	}

	//set camera view 
	target.setView(camera);

	//player instance drawn to level 
	playerInstance.DrawTo(target);

	//platform instance drawn to level 
	//platformInstance.DrawTo(target);

	for (int i = 0; i < platforms.size(); i++)
	{
		platforms[i].DrawTo(target);
	}
	//platform instance drawn to level 
	//platformInstance2.DrawTo(target);

	//remove camera view 
	target.setView(target.getDefaultView());
}

void LevelScreen::AddPlatform()
{
	//create the new platform 
	Platform newPlatform;

	sf::Vector2f newPosition;
	newPosition.y = highestPlatform - platformGap;

	//play area calculations
	int PLAY_AREA_WIDTH = 800;
	int minX = camera.getCenter().x - PLAY_AREA_WIDTH / 2;
	int maxX = camera.getCenter().x - PLAY_AREA_WIDTH / 2;

	//randomise platform x position
	maxX -= newPlatform.getHitBox().width;
	newPosition.x = rand() % (maxX - minX) + minX;

	//set new platform position 
	newPlatform.SetPosition(newPosition);

	//add the new platform to the list 
	platforms.push_back(newPlatform);

	//uodate platform gap 
	platformGap += platformGapIncrease;

	//update highest platform 
	highestPlatform = newPosition.y;
}
