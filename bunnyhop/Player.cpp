#include "Player.h"
#include "AssetManager.h"


Player::Player(sf::Vector2u screenSize)
	:AnimatingObject(AssetManager::RequestTexture("Assets/Graphics/PlayerAnimation.png"), 75, 100, 8.0f)
	,velocity(0.0f,0.0f)
	,speed(300.0f)
	,gravity(800.0f)
	,previousPosition()
{
	AddClip("Jump", 0, 1);

	PlayClip("Jump", false);

	//constructor for screen size, position the player in centre of screen 
	sf::Vector2f newPosition;
	newPosition.x = ((float)screenSize.x - sprite.getGlobalBounds().width) / 2.0f;
	newPosition.y = ((float)screenSize.y - sprite.getGlobalBounds().height) / 2.0f;
	sprite.setPosition(newPosition);
	previousPosition = newPosition;
}
void Player::Input()
{
	//player keybind input (x direction only)
	//start by zeroing out player x velocity
	velocity.x = 0.0f;
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::A))
	{
		//move player left
		velocity.x = -speed;

	}
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::D))
	{
		//move player right
		velocity.x = speed;

	}


}
void Player::Update(sf::Time frameTime)
{
	//calculate the new position 
	sf::Vector2f newPosition = sprite.getPosition() + velocity * frameTime.asSeconds();


	//setting position in collision 
	previousPosition = sprite.getPosition();

	//move the player to the new position
	sprite.setPosition(newPosition);

	AnimatingObject::Update(frameTime);

	//calculate the new velocity 
	velocity.y = velocity.y + gravity * frameTime.asSeconds();

}
void Player::HandleSolidCollision(sf::FloatRect otherHitbox)
{
	//TODO: if we are colliding with a platform 
	if (getHitBox().intersects(otherHitbox))
	{
		//TODO: AND if we were previously above the platform(falling down)
		float previousBottom = previousPosition.y + getHitBox().height;
		float platformTop = otherHitbox.top;
		if (previousBottom < platformTop)
		{
			//TODO: ste our upward velocity to a jump value.
			const float JUMP_VALUE =- 700; //negative to go up. adjust as needed
			velocity.y = JUMP_VALUE;
			PlayClip("Jump", false);
		}

	}
}
;