#pragma once
#include <map>
#include <SFML/Graphics.hpp>
#include "SpriteObject.h"

class AnimatingObject : public SpriteObject
{
private:
	struct Clip
	{
	public:
		int startFrame;
		int endFrame;
	};
	int frameWidth;
	int frameHeight;

	int currentFrame;
	sf::Time timeInFrame;

	float framesPerSecond;

	//codd for the clip #need to add string for clip that will be playing 
	std::map<std::string, Clip> clips;
	std::string currentClip;

	bool playing;
	bool looping;

	void UpdateSpriteTextureRect();

public:

	AnimatingObject(sf::Texture& newTexture, int newFrameWidth, int newFrameHeight, float newFPS);

	void Update(sf::Time frameTime);

	//added clip 
	void AddClip(std::string name, int startFrame, int endFrame);
	void PlayClip(std::string name, bool shouldLoop = true);

	void Pause();
	void Stop();
	void Resume();

};