#pragma once
#include <SFML/Graphics.hpp>
#include "Player.h"
#include "Platform.h"

class Game;

class LevelScreen
{

public:

	LevelScreen(Game* newGamePointer);

	void Input();
	void Update(sf::Time frameTime);
	void DrawTo(sf::RenderTarget& target);

private:
	//sf::RenderWindow window;

	Game* gamePointer;
	sf::View camera;

	Player playerInstance;
	//Platform platformInstance;
	//Platform platformInstance2;
	std::vector<Platform>platforms;
	float platformGap;
	float platformGapIncrease;
	float highestPlatform;
	float platformBuffer;

	void AddPlatform();
	

};

