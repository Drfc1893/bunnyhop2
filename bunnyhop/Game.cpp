#include "Game.h"

Game::Game()
	:window(sf::VideoMode::getDesktopMode(), "Bunny Hop", sf::Style::Titlebar | sf::Style::Close)
	, gameClock()
	,levelScreenInstance(this)
{
	//window setup
	window.setMouseCursorVisible(false);
	window.create(sf::VideoMode::getDesktopMode(), "Bunny Hop", sf::Style::Titlebar | sf::Style::Close);
}

void Game::RunGameLoop()
{
	//repeat as long as the window is open 
	while (window.isOpen())
	{
		Input();
		Update();
		Draw();
	}
}

void Game::Input()
{
	sf::Event event;
	while (window.pollEvent(event))
	{
		if (event.type == sf::Event::Closed)
		{
			window.close();
		}

		//close game if escape is pressed
		if (sf::Keyboard::isKeyPressed(sf::Keyboard::Escape))
		{
			window.close();
		}
	}

	//levelk screen inpout 
	levelScreenInstance.Input();
}

void Game::Update()
{
	sf::Time frameTime = gameClock.restart();
	levelScreenInstance.Update(frameTime);
}

void Game::Draw()
{
	window.clear();

	levelScreenInstance.DrawTo(window);


	window.display();
}

sf::RenderWindow & Game::GetWindow()
{
	// TODO: insert return statement here
	return window;
}
