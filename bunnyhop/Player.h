#pragma once
#include "AnimatingObject.h"

class Player : public AnimatingObject
{

public:
	Player(sf::Vector2u screenSize);

	//functiion to call for player code
	void Input();
	void Update(sf::Time frameTime);

	void HandleSolidCollision(sf::FloatRect otherHitbox);

private:
	//data 
	sf::Vector2f velocity;
	float speed;
	float gravity;

	//collision code
	sf::Vector2f previousPosition;


};