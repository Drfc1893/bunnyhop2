#include <SFML/Graphics.hpp>
#include "Game.h"
#include <stdlib.h>
#include <time.h>

int main()
{
	Game gameInstance;

	//initialize random seed 
	srand(time(NULL));
	//this will not end until the game is over 
	gameInstance.RunGameLoop();

	//if we get here, the loop exited, so end the program by returning 
	return 0;


}
