#pragma once
#include <SFML/Graphics.hpp>

class SpriteObject
{

public:

	//construct / destructors
	SpriteObject(sf::Texture& newTexture);

	//functions
	void DrawTo(sf::RenderTarget& target);

	sf::FloatRect getHitBox();




protected:

	//data 
	sf::Sprite sprite;

	sf::Vector2f CalculateCollisionDepth(sf::FloatRect otherHitBox);
};


