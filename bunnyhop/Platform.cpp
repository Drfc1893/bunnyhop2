#include "Platform.h"
#include "SpriteObject.h"
#include "AssetManager.h"

Platform::Platform()
	:SpriteObject(AssetManager::RequestTexture("Assets/Graphics/Platform.png"))
{

}
void Platform::SetPosition(sf::Vector2f newPosition)
{
	//set sprite position 
	sprite.setPosition(newPosition);
}
