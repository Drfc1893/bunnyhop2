#include "SpriteObject.h"

SpriteObject::SpriteObject(sf::Texture& newTexture) :sprite(newTexture)
{

}

//draw function
void SpriteObject::DrawTo(sf::RenderTarget& target)
{
	target.draw(sprite);
}

sf::FloatRect SpriteObject::getHitBox()
{
	return sf::FloatRect(sprite.getGlobalBounds());
}

sf::Vector2f SpriteObject::CalculateCollisionDepth(sf::FloatRect otherHitBox)
{
	sf::FloatRect thisHitbox = getHitBox();

	//calculate teh centre of each object 
	sf::Vector2f centerOther(otherHitBox.left + otherHitBox.width / 2.0f, otherHitBox.top + otherHitBox.height / 2.0f);
	sf::Vector2f centerThis(thisHitbox.left + thisHitbox.width / 2.0f, thisHitbox.top + thisHitbox.height / 2.0f);

	//return sf::Vector2f();

	//calculate the current distance between the two objects 
	float distanceX = centerOther.x - centerThis.x;
	float distanceY = centerOther.y - centerThis.y;

	//collision minimum(so half height a + half b height)
	//calculate the minimum collision distance between the two objects 
	float minDistanceX = otherHitBox.width / 2.0f + thisHitbox.width / 2.0f;
	float minDistanceY = otherHitBox.height / 2.0f + thisHitbox.height / 2.0f;

	//account for the negative psoitioning 
	if (distanceX < 0)
		minDistanceX = -minDistanceX;
	if (distanceY < 0)
		minDistanceY = -minDistanceY;

	//calcukate the depth by subtracting distacne from minimum distance 
	float depthX = minDistanceX - distanceX;
	float depthY = minDistanceY - distanceY;
	//return the calculated depth to the calling code 
	return sf::Vector2f(depthX, depthY);
}