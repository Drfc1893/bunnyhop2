#include "AnimatingObject.h"

AnimatingObject::AnimatingObject(sf::Texture& newTexture, int newFrameWidth, int newFrameHeight, float newFPS)
	:SpriteObject(newTexture)
	, frameWidth(newFrameWidth)
	, frameHeight(newFrameHeight)
	, framesPerSecond(newFPS)
	, currentFrame(0)
	, timeInFrame(sf::seconds(0.0f))
	, clips()
	, currentClip("")
	, playing(false)
	,looping(true)
{

}



void AnimatingObject::AddClip(std::string name, int startFrame, int endFrame)
{
	//create new animation 
	Clip& newClip = clips[name];

	//set up the settings for this animation
	newClip.startFrame = startFrame;
	newClip.endFrame = endFrame;

}

void AnimatingObject::PlayClip(std::string name, bool shouldLoop)
{

	//if the clip we're trying to play is alrerady playing,
	//ball out early 
	if (currentClip == name && playing ==true)
	{
		return;
	}

	//TODO: find the clip's information in the map
	auto pairFound = clips.find(name);

	//TODO: if the clip exists 
	if (pairFound != clips.end())
	{
		//TODO: set up teh animation based on the clip 
		currentClip = name;
		currentFrame = pairFound->second.startFrame;
		timeInFrame = sf::seconds(0.0f);
		playing = true;
		looping = shouldLoop;
		UpdateSpriteTextureRect();
	}


}

void AnimatingObject::Pause()
{
	playing = false;
}

void AnimatingObject::Stop()
{
	playing = false;

	//reset to first frame of the animation
	Clip& thisClip = clips[currentClip];
	currentFrame = thisClip.startFrame;

	UpdateSpriteTextureRect();

}

void AnimatingObject::Resume()
{
	if (!currentClip.empty())
		playing = true;
}

void AnimatingObject::Update(sf::Time frameTime)
{
	//dobn update if we arent actually playing 
	if (!playing)
		return;

	//if ti is time for a new frame
	timeInFrame += frameTime;
	sf::Time timePerFrame = sf::seconds(1.0f / framesPerSecond);
	if (timeInFrame >= timePerFrame)
	{
		//update the current frame 
		++currentFrame;
		timeInFrame = sf::seconds(0);

		//if we got to the end of the clip 
		Clip& thisClip = clips[currentClip];
		if (currentFrame > thisClip.endFrame)
		{

			//if we are supposed to loop..........
			if (looping == true)
			{

				//return to the beginning of the clip
				currentFrame = thisClip.startFrame;
			}
			else//we are not supposed to loop....
			{
				//stay on the last fram e
				currentFrame = thisClip.endFrame;
				playing = false;
			}
			
		}
		//update our sprite to use the correct frame of the texture 
		UpdateSpriteTextureRect();
	}
}

void AnimatingObject::UpdateSpriteTextureRect()
{
	//set sprite recangle to match this clip's first frame 
	int numFramesX = sprite.getTexture()->getSize().x / frameWidth;
	int xFrameIndex = currentFrame % numFramesX;
	int yFrameIndex = currentFrame / numFramesX;
	sf::IntRect textureRect;
	textureRect.left = xFrameIndex * frameWidth;
	textureRect.top = yFrameIndex * frameHeight;
	textureRect.width = frameWidth;
	textureRect.height = frameHeight;
	sprite.setTextureRect(textureRect);
}

/*void PlayClip(std::string name, bool shouldLoop)
{
	//if the clip we're trying to play is alrerady playing,
	//ball out early 
	if (currentClip == name)
	{
		return;
	}

	//Find the clip's information in map 
	auto pairFound = clips.find(name);

	//if the clip exists...
	if (pairFound != clips.end())
	{
		//set up the animation based on the clip 
		currentClip = name;
		currentFrame = pairFound->second.startFrame;
		timeInFrame = sf::seconds(0.0f);
		playing = true;
		looping = shouldLoop;

		//update sprite text 
		UpdateSpriteTextureRect();

	}
}*/